var stack1,
    stack2,
    stack3,
    params,
    nickname,
    difficulty,
    score,
    dragSource,
    dragTarget,
    scoreDiv;

function initGame() {
    params = window.location.href.substring(window.location.href.indexOf("?"));
    difficulty = parseInt(params.substring(1, params.indexOf("+")));
    nickname = params.substring(difficulty.length + 2);
    score = 0;

    stack1 = document.getElementById("stack1");
    stack2 = document.getElementById("stack2");
    stack3 = document.getElementById("stack3");

    scoreDiv = document.getElementById("score");
    scoreDiv.innerHTML = score;

    stack1.appendChild(makeDisk(0));
    stack2.appendChild(makeDisk(0));
    stack3.appendChild(makeDisk(0));

    if (document.cookie.indexOf(params) > -1) {
        var cookie = document.cookie.split(",");
        console.log(cookie);
        var s1 = parseInt(cookie[1]);
        fillStack(stack1, s1);
        var s2 = parseInt(cookie[2]);
        fillStack(stack2, s2);
        var s3 = parseInt(cookie[3]);
        fillStack(stack3, s3);
    } else {
        for (var i = 1; i < difficulty + 1; i++) {
            var disk_div = makeDisk(i);
            stack1.appendChild(disk_div);
        }
    }

    setTop(stack1);
    setTop(stack2);
    setTop(stack3);
}

function fillStack(stack, size) {
    for (var i = 1; i < size + 1; i++) {
        var disk_div = makeDisk(i);
        stack.appendChild(disk_div);
    }
}

function setTop(stack) {
    stack.lastElementChild.setAttribute("draggable", "True");
    stack.lastElementChild.setAttribute("ondrop", "drop(event)");
    stack.lastElementChild.setAttribute("ondragover", "dragOver(event)");
    stack.lastElementChild.setAttribute("ondragstart", "drag(event)");
    stack.lastElementChild.style.backgroundColor = "red";

    stack.firstElementChild.style.backgroundColor = "black";
}

function clear(element) {
    element.removeAttribute("draggable");
    element.removeAttribute("ondrop");
    element.removeAttribute("ondragover");
    element.removeAttribute("ondragstart");
    element.style.backgroundColor = "#3e3e3e;";
}

function makeDisk(i) {
    var disk_div = document.createElement("div");

    disk_div.id = "disk" + i;
    disk_div.className = "disk";
    disk_div.style.width = 100 - 15 * i + "%";

    return disk_div;
}

function dragOver(e) {
    e.preventDefault();
    if (e.target != dragSource) dragTarget = e.target;
}

function drag(e) {
    dragSource = e.target;
}

function drop(e) {
    e.preventDefault();
    var w1 = parseInt(dragSource.parentNode.lastChild.style.width.substring(0, dragSource.style.width.length - 1));
    var w2 = parseInt(dragTarget.style.width.substring(0, dragTarget.style.width.length - 1));

    if (w1 < w2) {
        dragTarget.parentNode.appendChild(dragSource);
        dragTarget.removeAttribute("draggable");

        clear(dragTarget);

        setTop(stack1);
        setTop(stack2);
        setTop(stack3);

        score++;
        scoreDiv.innerHTML = score;
    }

    if (stack3.getElementsByClassName("disk").length > difficulty) {
        alert("You win!");
    }

    document.cookie = params
        + "," + (stack1.getElementsByClassName("disk").length - 1)
        + "," + (stack2.getElementsByClassName("disk").length - 1)
        + "," + (stack3.getElementsByClassName("disk").length - 1);
}