function startGame() {
    var difficulty = document.getElementById("difficulty").value;
    var nickname = document.getElementById("nickname").value;

    if (nickname.length > 3) {
        window.location = "play.html?" + difficulty + "+" + nickname;
    } else {
        var form = document.getElementById("form");
        var nicknameError = document.createElement("div");
        nicknameError.innerHTML = "Please choose a nickname with more than 3 letters!";

        form.appendChild(nicknameError);
    }
}