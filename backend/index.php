<!doctype html>
<html>
<head>
    <meta charset="utf-8">
</head>

<body>
<?php
session_start();

if (!isset($_SESSION["userid"]) and !empty($_SESSION['userid'])) {
    header("Location: login.php");
    die();
}
?>
<p><a href="offers.php">Offers</a></p>
<p><a href="requests.php">Requests</a></p>
<?php
#if ($_SESSION["usertype"] == "driver") {
    echo "<p><a href=\"offers.php\">My Offers</a></p>";
#}

#if ($_SESSION["usertype"] == "passenger") {
    echo "<p><a href=\"requests.php\">My Requests</a></p>";
#}
?>

<a href="modules/logout.php">Logout</a>
</body>
</html>
