<?php
include "modules/db_connect.php";

# Offered Routes
$offers = "
SELECT firstname, lastname, source_location, target_location, date_time 
FROM driver 
NATURAL JOIN route 
NATURAL JOIN user";

$result = $db->query($offers);

include "modules/display_route.php";
