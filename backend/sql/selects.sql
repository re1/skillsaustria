-- All offers
SELECT firstname, lastname, source_location, target_location, date_time
FROM driver
NATURAL JOIN route
NATURAL JOIN user;
-- All requests
SELECT firstname, lastname, source_location, target_location, date_time
FROM passenger
NATURAL JOIN route
NATURAL JOIN user;
-- My Offers
SELECT firstname, lastname, source_location, target_location, date_time
FROM driver
NATURAL JOIN route
NATURAL JOIN user
WHERE UID = 'username';