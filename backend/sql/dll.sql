DROP DATABASE IF EXISTS carsharing;
CREATE DATABASE carsharing;

USE carsharing;

CREATE TABLE user(
  userid  VARCHAR(255),
  userpw  VARCHAR(24),
  firstname VARCHAR(255),
  lastname  VARCHAR(255),
  state   VARCHAR(255),
  PRIMARY KEY (userid)
) ENGINE = INNODB;

CREATE TABLE driver(
  userid  VARCHAR(255),
  PRIMARY KEY (userid),
  FOREIGN KEY (userid) REFERENCES user(userid) ON UPDATE CASCADE ON DELETE CASCADE
) ENGINE = INNODB;

CREATE TABLE passenger(
  userid  VARCHAR(255),
  PRIMARY KEY (userid),
  FOREIGN KEY (userid) REFERENCES user(userid) ON UPDATE CASCADE ON DELETE CASCADE
) ENGINE = INNODB;

CREATE TABLE route(
  userid      VARCHAR(255),
  date_time   DATETIME,
  source_location   VARCHAR(255),
  target_location   VARCHAR(255),
  PRIMARY KEY (userid, date_time, source_location, target_location),
  FOREIGN KEY (userid) REFERENCES user(userid) ON UPDATE CASCADE ON DELETE CASCADE
) ENGINE = INNODB;

CREATE TABLE rating(
  giveuid  VARCHAR(255),
  takeuid  VARCHAR(255),
  rating    TINYINT(255),
  PRIMARY KEY (giveuid, takeuid),
  FOREIGN KEY (giveuid) REFERENCES user(userid) ON UPDATE CASCADE ON DELETE CASCADE,
  FOREIGN KEY (takeuid) REFERENCES user(userid) ON UPDATE CASCADE ON DELETE CASCADE
) ENGINE = INNODB;

INSERT INTO user VALUES (
  'user1',
  'pass1',
  'hans',
  'peterson',
  'inactive'
);

INSERT INTO driver VALUES (
  'user1'
);

INSERT INTO user VALUES (
  'user2',
  'pass2',
  'claudia',
  'graf',
  'inactive'
);

INSERT INTO passenger VALUES (
  'user2'
);

INSERT INTO route VALUES (
  'user1',
  '2016-12-01 12:01:00',
  'airport',
  'city'
);

INSERT INTO route VALUES (
  'user2',
  '2017-01-12 21:10:00',
  'airport',
  'city'
);

INSERT INTO rating VALUES (
  'user2',
  'user1',
  4
);