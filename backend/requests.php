<?php
include "modules/db_connect.php";
# Requested Routes
$requests = "
SELECT firstname, lastname, source_location, target_location, date_time 
FROM passenger 
NATURAL JOIN route 
NATURAL JOIN user";

$result = $db->query($requests);

include "modules/display_route.php";