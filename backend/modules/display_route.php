<?php
if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
        echo "<p>"
            . $row["firstname"]. " "
            . $row["lastname"]. " "
            . $row["source_location"]. " -> "
            . $row["target_location"] . " "
            . $row["date_time"]
            . "</p>";
    }
} else {
    echo "0 results";
}

$db->close();