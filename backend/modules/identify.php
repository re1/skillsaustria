<?php
$username = "skillsaustria";
$password = "skillsaustria";
$database = "carsharing";
$server = "localhost";

$db = new mysqli($server, $username, $password, $database);

if ($db->connect_error) die("Connection failed");
else echo "<p>Connected!</p>";

$login_query = "SELECT userid FROM user WHERE userid =" . "'" . $_POST["username"] . "'" . "AND userpw = " . "'" . $_POST["password"] . "'";
$result = $db->query($login_query);


if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
        $_SESSION["userid"] = $row["userid"];
    }
} else {
    echo "Login failed";
}

$driver_query = "SELECT userid FROM driver WHERE userid =" . "'" . $_POST["username"] . "'";
$passenger_query = "SELECT userid FROM passenger WHERE userid =" . "'" . $_POST["username"] . "'";

$result = $db->query($driver_query);
if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
        $_SESSION["usertype"] = "driver";
    }
}

$result = $db->query($passenger_query);
if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
        $_SESSION["usertype"] = "passenger";
    }
}

$db->close();

if (isset($_SESSION["userid"]) && !empty($_SESSION['userid'])) {
    header("Location: index.php");
} else {
    header("Location: login.php");
}

die();
